/* Copyright (c) 2015
* by Charles River Development, Inc., Burlington, MA
*
* This software is furnished under a license and may be used only in
* accordance with the terms of such license. This software may not be
* provided or otherwise made available to any other party. No title to
* nor ownership of the software is hereby transferred.
*
* This software is the intellectual property of Charles River Development, Inc.,
* and is protected by the copyright laws of the United States of America.
* All rights reserved internationally.
*
*/

package rubiks;

import static org.junit.Assert.*;

import java.awt.Color;
import java.util.HashMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import rubiks.Face.FaceType;
import rubiks.Square.SquarePosition;

public class TestCubeFace
{
    static final Color RED    =  Color.RED, 
                       WHITE  =  Color.WHITE, 
                       BLUE   =  Color.BLUE,
                       GREEN  =  Color.GREEN, 
                       ORANGE =  Color.ORANGE,
                       YELLOW =  Color.YELLOW;    
    Face face;
    
    @Before
    public void setUp() throws Exception
    {
        face = new Face(FaceType.FRONT, RED);
    }

    @After
    public void tearDown() throws Exception
    {
        face = null;
    }
    
    /////////////////////////////////////////////////
    //
    //   Test Cases
    //
    /////////////////////////////////////////////////

    @Test
    public void testNewFaceHasCorrectSquares()
    {
        for (SquarePosition position : SquarePosition.values()) {
            assertNotNull("Should have square in " + position.toString() + " position", face.getSquare(position));
        }
    }
    
    @Test
    public void testNewFaceHasCorrectColor() 
    {
        assertEquals("Should have 9 squares per face", face.getSquareList().size(), 9);
        
        for (Square square : face.getSquareList()) {
            assertEquals("All squares should be same color", square.getColor(), RED);
        }
    }
    
    @Test
    public void testFaceInsertSquares()
    {
        HashMap<SquarePosition, Square> insertMap = new HashMap<SquarePosition, Square>();
        insertMap.put(SquarePosition.TOP_LEFT, new Square(SquarePosition.TOP_LEFT, BLUE));
        insertMap.put(SquarePosition.TOP_MIDDLE, new Square(SquarePosition.TOP_MIDDLE, BLUE));
        insertMap.put(SquarePosition.TOP_RIGHT, new Square(SquarePosition.TOP_RIGHT, BLUE));
        
        face.insertSquares(insertMap);
        
        assertEquals(face.getTopLeftSquare().getColor(), BLUE);
        assertEquals(face.getTopMiddleSquare().getColor(), BLUE);
        assertEquals(face.getTopRightSquare().getColor(), BLUE);
        
        assertEquals(face.getCenterLeftSquare().getColor(), RED);
        assertEquals(face.getCenterMiddleSquare().getColor(), RED);
        assertEquals(face.getCenterRightSquare().getColor(), RED);
        
        assertEquals(face.getBottomLeftSquare().getColor(), RED);
        assertEquals(face.getBottomMiddleSquare().getColor(), RED);
        assertEquals(face.getBottomRightSquare().getColor(), RED); 
        
        insertMap.clear();
        
        insertMap.put(SquarePosition.CENTER_LEFT, new Square(SquarePosition.CENTER_LEFT, GREEN));
        insertMap.put(SquarePosition.CENTER_MIDDLE, new Square(SquarePosition.CENTER_MIDDLE, GREEN));
        insertMap.put(SquarePosition.CENTER_RIGHT, new Square(SquarePosition.CENTER_RIGHT, GREEN));
        
        face.insertSquares(insertMap);
        
        assertEquals(face.getTopLeftSquare().getColor(), BLUE);
        assertEquals(face.getTopMiddleSquare().getColor(), BLUE);
        assertEquals(face.getTopRightSquare().getColor(), BLUE);
        
        assertEquals(face.getCenterLeftSquare().getColor(), GREEN);
        assertEquals(face.getCenterMiddleSquare().getColor(), GREEN);
        assertEquals(face.getCenterRightSquare().getColor(), GREEN);
        
        assertEquals(face.getBottomLeftSquare().getColor(), RED);
        assertEquals(face.getBottomMiddleSquare().getColor(), RED);
        assertEquals(face.getBottomRightSquare().getColor(), RED);
        
        insertMap.clear();
        
        insertMap.put(SquarePosition.CENTER_LEFT, new Square(SquarePosition.CENTER_LEFT, YELLOW));
        insertMap.put(SquarePosition.CENTER_MIDDLE, new Square(SquarePosition.CENTER_MIDDLE, YELLOW));
        insertMap.put(SquarePosition.CENTER_RIGHT, new Square(SquarePosition.CENTER_RIGHT, YELLOW));
        insertMap.put(SquarePosition.BOTTOM_LEFT, new Square(SquarePosition.BOTTOM_LEFT, WHITE));
        insertMap.put(SquarePosition.BOTTOM_MIDDLE, new Square(SquarePosition.BOTTOM_MIDDLE, WHITE));
        insertMap.put(SquarePosition.BOTTOM_RIGHT, new Square(SquarePosition.BOTTOM_RIGHT, WHITE));
        
        face.insertSquares(insertMap);
        
        assertEquals(face.getTopLeftSquare().getColor(), BLUE);
        assertEquals(face.getTopMiddleSquare().getColor(), BLUE);
        assertEquals(face.getTopRightSquare().getColor(), BLUE);
        
        assertEquals(face.getCenterLeftSquare().getColor(), YELLOW);
        assertEquals(face.getCenterMiddleSquare().getColor(), YELLOW);
        assertEquals(face.getCenterRightSquare().getColor(), YELLOW);
        
        assertEquals(face.getBottomLeftSquare().getColor(), WHITE);
        assertEquals(face.getBottomMiddleSquare().getColor(), WHITE);
        assertEquals(face.getBottomRightSquare().getColor(), WHITE);
    }
    
    @Test
    public void testFaceSingleTurn()
    {
        face = setupModifiedFace();
        
        verifyOrigFaceConfig();        
        
        face.turn();
        face.turn();
        face.turn();
        face.turn();
        
        // Four turns will return to same position
        verifyOrigFaceConfig();
        
        face.turn();
        face.turnPrime();
        
        // Quarter turn CW an CCw, same position
        verifyOrigFaceConfig();
        
        face.turnPrime();
        face.turnPrime();
        face.turn();
        face.turn();
        
        // Still same
        verifyOrigFaceConfig();
        
        face.turnPrime();
        
        // One quarter turn
        assertEquals(face.getBottomLeftSquare().getColor(), RED);
        assertEquals(face.getCenterLeftSquare().getColor(), ORANGE);
        assertEquals(face.getTopLeftSquare().getColor(), BLUE);
        
        assertEquals(face.getBottomMiddleSquare().getColor(), GREEN);
        assertEquals(face.getCenterMiddleSquare().getColor(), YELLOW);
        assertEquals(face.getTopMiddleSquare().getColor(), WHITE);
        
        assertEquals(face.getBottomRightSquare().getColor(), RED);
        assertEquals(face.getCenterRightSquare().getColor(), ORANGE);
        assertEquals(face.getTopRightSquare().getColor(), BLUE);
        
        face.turn();
        face.turn();
        
        // One quarter turn in opposite direction
        assertEquals(face.getTopLeftSquare().getColor(), RED);
        assertEquals(face.getTopMiddleSquare().getColor(), GREEN);
        assertEquals(face.getTopRightSquare().getColor(), RED);
        
        assertEquals(face.getCenterLeftSquare().getColor(), ORANGE);
        assertEquals(face.getCenterMiddleSquare().getColor(), YELLOW);
        assertEquals(face.getCenterRightSquare().getColor(), ORANGE);
        
        assertEquals(face.getBottomLeftSquare().getColor(), BLUE);
        assertEquals(face.getBottomMiddleSquare().getColor(), WHITE);
        assertEquals(face.getBottomRightSquare().getColor(), BLUE); 
    }
    
    ////////////////////////////////////////////////
    //
    // Utilities
    //
    /////////////////////////////////////////////////
    
    private void verifyOrigFaceConfig()
    {
        assertEquals(face.getTopLeftSquare().getColor(), RED);
        assertEquals(face.getTopMiddleSquare().getColor(), ORANGE);
        assertEquals(face.getTopRightSquare().getColor(), BLUE);
        
        assertEquals(face.getCenterLeftSquare().getColor(), GREEN);
        assertEquals(face.getCenterMiddleSquare().getColor(), YELLOW);
        assertEquals(face.getCenterRightSquare().getColor(), WHITE);
        
        assertEquals(face.getBottomLeftSquare().getColor(), RED);
        assertEquals(face.getBottomMiddleSquare().getColor(), ORANGE);
        assertEquals(face.getBottomRightSquare().getColor(), BLUE);           
    }

    private Face setupModifiedFace()
    {
        HashMap<SquarePosition, Square> map = new HashMap<SquarePosition, Square>();
        
        map.put(SquarePosition.TOP_LEFT, new Square(SquarePosition.TOP_LEFT, RED));
        map.put(SquarePosition.TOP_MIDDLE, new Square(SquarePosition.TOP_MIDDLE, ORANGE));
        map.put(SquarePosition.TOP_RIGHT, new Square(SquarePosition.TOP_RIGHT, BLUE));       

        map.put(SquarePosition.CENTER_LEFT, new Square(SquarePosition.CENTER_LEFT, GREEN));
        map.put(SquarePosition.CENTER_MIDDLE, new Square(SquarePosition.CENTER_MIDDLE, YELLOW));
        map.put(SquarePosition.CENTER_RIGHT, new Square(SquarePosition.CENTER_RIGHT, WHITE));        

        map.put(SquarePosition.BOTTOM_LEFT, new Square(SquarePosition.BOTTOM_LEFT, RED));
        map.put(SquarePosition.BOTTOM_MIDDLE, new Square(SquarePosition.BOTTOM_MIDDLE, ORANGE));
        map.put(SquarePosition.BOTTOM_RIGHT, new Square(SquarePosition.BOTTOM_RIGHT, BLUE));
        
        return new Face(FaceType.FRONT, map);
    }

}
