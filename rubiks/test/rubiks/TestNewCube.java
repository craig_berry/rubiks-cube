/* Copyright (c) 2015
* by Charles River Development, Inc., Burlington, MA
*
* This software is furnished under a license and may be used only in
* accordance with the terms of such license. This software may not be
* provided or otherwise made available to any other party. No title to
* nor ownership of the software is hereby transferred.
*
* This software is the intellectual property of Charles River Development, Inc.,
* and is protected by the copyright laws of the United States of America.
* All rights reserved internationally.
*
*/

package rubiks;

import static org.junit.Assert.*;

import java.awt.Color;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import rubiks.Face.FaceType;

public class TestNewCube
{
    Cube cube;
    
    @Before
    public void setUp() throws Exception
    {
        cube = Cube.createNewCube();
    }

    @After
    public void tearDown() throws Exception
    {
        cube = null;
    }

    @Test
    public void testNewCubeHasCorrectFaces()
    {
        for (FaceType faceType : FaceType.values()) {
            assertNotNull("Null returned for " + faceType.toString() + " face", cube.getFace(faceType));
        }
    }
    
    @Test
    public void testNewCubeIsSolvedAfterInitialization()
    {
        assertTrue("Each face should be different color", cube.verifyFaceColors());
        assertTrue("Cube should be solved initially", cube.isSolved());
    }
    
    @Test
    public void testNewCubeHasCorrectSquares()
    {
        ArrayList<Square> squareList = cube.getSquareList();
        assertEquals("Should have eactly 54 squares", squareList.size(), 6*9);
        
        for (Color color : Cube.getCubeColorSet()) {
            int count = 0;
            
            for (Square square : squareList) {                
                if (square.getColor().equals(color)) {
                    count++;
                }
            }            
            assertEquals("There should be 9 " + color.toString() + " squares", count, 9);
        }
    }
    
    @Test
    public void testCubeEquals() throws Exception {
        Cube otherCube = Cube.createNewCube();
        
        assertTrue("Should be equal", cube.equals(otherCube));
        assertTrue("Should be equal", otherCube.equals(cube));
    }
    
}
