package rubiks;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import rubiks.Face.FaceType;
import rubiks.Square.SquarePosition;

public class Cube
{
    private HashMap<FaceType, Face> faceMap;

    public Cube(HashMap<FaceType, Face> faceMap)
    {
        this.faceMap = faceMap;
    }
    
    ////////////////////////////////
    //
    //  Setup
    //
    /////////////////////////////////

    public static Cube createNewCube() throws Exception
    {
        HashMap<FaceType, Face> faceMap = new HashMap<FaceType, Face>();
        Iterator<Color> colorIterator = getCubeColorSet().iterator();
        
        for (FaceType faceType : FaceType.values()) {
            Color color = colorIterator.hasNext() ? colorIterator.next() : Color.BLACK;
            faceMap.put(faceType, new Face(faceType, color));
        }
        
        if (faceMap.size() != 6) {
            throw new Exception("createNewCube() did not create all 6 faces");
        }
        
        for (Face face : faceMap.values()) {
            if (face.getColor().equals(Color.BLACK)) {
                throw new Exception("createNewCube() failed to get color for " + face.faceType);
            }
        }
        
        return new Cube(faceMap);        
    }   
    
    ////////////////////////////////
    //
    //  Logic
    //
    /////////////////////////////////
    
    protected boolean isSolved() 
    {
        for (Face face : faceMap.values()) {
            if (!face.isSolved()) {
                return false;
            }
        }
        return true;
    }
    
    protected boolean verifyFaceColors() 
    {
        HashSet<Color> colorSet = new HashSet<Color>();
        
        for (Face face : faceMap.values()) {
            colorSet.add(face.getColor());
        }
        
        return colorSet.equals(getCubeColorSet());
    }
    
    protected ArrayList<Square> getSquareList()
    {
        ArrayList<Square> squareList = new ArrayList<Square>();
        for (Face face : faceMap.values()) {
            squareList.addAll(face.getSquareList());
        }
        
        return squareList;
    }
    
    ////////////////////////////////
    //
    //  Turns
    //
    /////////////////////////////////
    
    public void frontTurn() 
    {
        turnFace(FaceType.FRONT, false);
    }
    
    public void frontTurnPrime() 
    {
        turnFace(FaceType.FRONT, true);
    }
    
    public void backTurn() 
    {
        turnFace(FaceType.BACK, false);
    }
    
    public void backTurnPrime() 
    {
        turnFace(FaceType.BACK, true);
    }
    
    public void topTurn() throws Exception 
    {                
       Face topFace   = turnFace(FaceType.TOP, false);
       Face rightFace = insertSquaresFromToFace(getBackFace(), TOP_ROW, getRightFace(), TOP_ROW);
       Face frontFace = insertSquaresFromToFace(getRightFace(), TOP_ROW, getFrontFace(), TOP_ROW);
       Face leftFace  = insertSquaresFromToFace(getFrontFace(), TOP_ROW, getLeftFace(), TOP_ROW);
       Face backFace  = insertSquaresFromToFace(getLeftFace(), TOP_ROW, getBackFace(), TOP_ROW);
       
       update(FaceType.TOP, topFace);
       update(FaceType.RIGHT, rightFace);
       update(FaceType.FRONT, frontFace);
       update(FaceType.LEFT, leftFace);
       update(FaceType.BACK, backFace);
    }

    public void topTurnPrime() throws Exception
    {
        Face topFace   = turnFace(FaceType.TOP, true);
        Face rightFace = insertSquaresFromToFace(getFrontFace(), TOP_ROW, getRightFace(), TOP_ROW);
        Face backFace = insertSquaresFromToFace(getRightFace(), TOP_ROW, getBackFace(), TOP_ROW);
        Face leftFace  = insertSquaresFromToFace(getBackFace(), TOP_ROW, getLeftFace(), TOP_ROW);
        Face frontFace  = insertSquaresFromToFace(getLeftFace(), TOP_ROW, getFrontFace(), TOP_ROW);
        
        update(FaceType.TOP, topFace);
        update(FaceType.RIGHT, rightFace);
        update(FaceType.FRONT, frontFace);
        update(FaceType.LEFT, leftFace);
        update(FaceType.BACK, backFace);
    }
    
    public void bottomTurn() 
    {
        turnFace(FaceType.BOTTOM, false);
    }
    
    public void bottomTurnPrime() 
    {
        turnFace(FaceType.BOTTOM, true);
    }
    
    public void leftTurn() 
    {
        turnFace(FaceType.LEFT, false);
    }
    
    public void leftTurnPrime() 
    {
        turnFace(FaceType.LEFT, true);
    }
    
    public void rightTurn() 
    {
        turnFace(FaceType.RIGHT, false);
    }
    
    public void rightTurnPrime() 
    {
        turnFace(FaceType.RIGHT, true);
    }
    
    ////////////////////////////////
    //
    //  Face Getters
    //
    /////////////////////////////////
      
    public Face getTopFace()
    {
        return getFace(FaceType.TOP);
    }
    
    public Face getBottomFace()
    {
        return getFace(FaceType.BOTTOM);
    }
    
    public Face getLeftFace()
    {
        return getFace(FaceType.LEFT);
    }
    
    public Face getRightFace()
    {
        return getFace(FaceType.RIGHT);
    }
    
    public Face getFrontFace()
    {
        return getFace(FaceType.FRONT);
    }
    
    public Face getBackFace()
    {
        return getFace(FaceType.BACK);
    }

    ////////////////////////////////
    //
    //  Static data
    //
    /////////////////////////////////
    
    // Rows
    public static final SquarePosition[] TOP_ROW         = new SquarePosition[] { SquarePosition.TOP_LEFT, SquarePosition.TOP_MIDDLE, SquarePosition.TOP_RIGHT };
    public static final SquarePosition[] TOP_ROW_INV     = new SquarePosition[] { SquarePosition.TOP_RIGHT, SquarePosition.TOP_MIDDLE, SquarePosition.TOP_LEFT };
    public static final SquarePosition[] CENTER_ROW      = new SquarePosition[] { SquarePosition.CENTER_LEFT, SquarePosition.CENTER_MIDDLE, SquarePosition.CENTER_RIGHT };
    public static final SquarePosition[] CENTER_ROW_INV  = new SquarePosition[] { SquarePosition.CENTER_RIGHT, SquarePosition.CENTER_MIDDLE, SquarePosition.CENTER_LEFT };
    public static final SquarePosition[] BOTTOM_ROW      = new SquarePosition[] { SquarePosition.BOTTOM_LEFT, SquarePosition.BOTTOM_MIDDLE, SquarePosition.BOTTOM_RIGHT };
    public static final SquarePosition[] BOTTOM_ROW_INV  = new SquarePosition[] { SquarePosition.BOTTOM_RIGHT, SquarePosition.BOTTOM_MIDDLE, SquarePosition.BOTTOM_LEFT };
    
    // Cols   
    public static final SquarePosition[] LEFT_COL        = new SquarePosition[] { SquarePosition.TOP_LEFT, SquarePosition.CENTER_LEFT, SquarePosition.BOTTOM_LEFT };
    public static final SquarePosition[] LEFT_COL_INV    = new SquarePosition[] { SquarePosition.BOTTOM_LEFT, SquarePosition.CENTER_LEFT, SquarePosition.TOP_LEFT };
    public static final SquarePosition[] MIDDLE_COL      = new SquarePosition[] { SquarePosition.TOP_MIDDLE, SquarePosition.CENTER_MIDDLE, SquarePosition.BOTTOM_MIDDLE };
    public static final SquarePosition[] MIDDLE_COL_INV  = new SquarePosition[] { SquarePosition.BOTTOM_MIDDLE, SquarePosition.CENTER_MIDDLE, SquarePosition.TOP_MIDDLE };
    public static final SquarePosition[] RIGHT_COL       = new SquarePosition[] { SquarePosition.TOP_RIGHT, SquarePosition.CENTER_RIGHT, SquarePosition.BOTTOM_RIGHT};
    public static final SquarePosition[] RIGHT_COL_INV   = new SquarePosition[] { SquarePosition.BOTTOM_RIGHT, SquarePosition.CENTER_RIGHT, SquarePosition.TOP_RIGHT };
    
    protected static HashSet<Color> getCubeColorSet()
    {
        HashSet<Color> colorSet = new HashSet<Color>();
        
        colorSet.add(Color.BLUE);
        colorSet.add(Color.GREEN);
        colorSet.add(Color.YELLOW);
        colorSet.add(Color.WHITE);
        colorSet.add(Color.RED);
        colorSet.add(Color.ORANGE);
        
        return colorSet;
    }
    
    ////////////////////////////////
    //
    //  Internals
    //
    /////////////////////////////////
    
    protected Face turnFace(FaceType faceType, boolean isPrime)
    {
        Face face = getFace(faceType);
        
        if (isPrime) {
            face.turnPrime();
        }
        else {
            face.turn();
        }
        
        return face.clone();
    }
    
    protected Face getFace(FaceType faceType)
    {
        return faceMap.get(faceType).clone();
    }
    
    @SuppressWarnings("unchecked")
    private void update(FaceType faceType, Face face) {
        faceMap.put(faceType, face.clone());
        faceMap = (HashMap<FaceType, Face>) faceMap.clone();
    }
    
    private Face insertSquaresFromToFace(Face fromFace, SquarePosition[] fromSquares, Face toFace, SquarePosition[] toSquares) throws Exception
    {
        if (fromSquares.length != toSquares.length) {
            throw new Exception("From to square list must be same length");
        }

        HashMap<SquarePosition, Square> insertMap = new HashMap<SquarePosition, Square>();
                
        for (int i = 0; i < fromSquares.length; i++) {
            insertMap.put(toSquares[i], fromFace.getSquare(fromSquares[i]));
        }
        
        toFace.insertSquares(insertMap);
        
        return toFace.clone();
    }    
    
    @Override
    public boolean equals(Object o) {
        
        if (this == o) {
            return true;
        }
        
        if(o == null || getClass() != o.getClass()) {
            return false;        
        }
        
        Cube that = (Cube)o;
        
        for (FaceType faceType : FaceType.values()) {
            for (SquarePosition squarePosition : SquarePosition.values()) {
                Square thisSq = this.getFace(faceType).getSquare(squarePosition);
                Square thatSq = that.getFace(faceType).getSquare(squarePosition);
                
                if (!thisSq.equals(thatSq)) {
                    return false;
                }
            }
        }
        return true;
    }
}
