
package rubiks;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;

import rubiks.Square.SquarePosition;

public class Face
{
    FaceType faceType;
    HashMap<SquarePosition, Square> squareMap;
    

    public Face(FaceType faceType, HashMap<SquarePosition, Square> squareMap)
    {
        this.faceType = faceType;
        this.squareMap = squareMap;
    }
    
    public Face(FaceType faceType, Color squareColor)
    {
        this.faceType = faceType;
        this.squareMap = createSquareMap(squareColor);
    }
        
    public Color getColor()
    {
        return getCenterMiddleSquare().getColor();
    }  
    
    @Override
    public Face clone() {
        return new Face(faceType, getSquareMap());        
    }
    
    ///////////////////////////////////////
    //
    // Setup
    //
    ///////////////////////////////////////   


    private HashMap<SquarePosition, Square> createSquareMap(Color squareColor)
    {
        HashMap<SquarePosition, Square> squareMap = new HashMap<Square.SquarePosition, Square>();
        
        for (SquarePosition position : SquarePosition.values()) {
            squareMap.put(position, new Square(position, squareColor));
        }
        
        return squareMap;
    }

    ///////////////////////////////////////
    //
    // Logic
    //
    ///////////////////////////////////////
    
    protected boolean isSolved()
    {
        Color faceColor = getColor();
        
        for (SquarePosition position : SquarePosition.values()) {
            if (!getSquare(position).getColor().equals(faceColor)) {
                return false;
            }
        }
        return true;
    }

    // Return array list of all squares in the face, basically contains color info
    public ArrayList<Square> getSquareList()
    {
        ArrayList<Square> squareList = new ArrayList<Square>();
        for (Square square : squareMap.values()) {
            squareList.add(square.clone());
        }
        return squareList;
    }
    
    // Returns cloned version of square map on face
    private HashMap<SquarePosition, Square> getSquareMap()
    {
        HashMap<SquarePosition, Square> map = new HashMap<SquarePosition, Square>();
        
        for (SquarePosition position : squareMap.keySet()) {
            map.put(position, squareMap.get(position).clone());
        }
        
        return map;
    }

    ///////////////////////////////////////
    //
    // Turns
    //
    ///////////////////////////////////////
    
    public void turn()
    {
        turnInternal(false);
    }

    public void turnPrime()
    {
        turnInternal(true);        
    }    
    
    protected void turnInternal(boolean isPrime)
    {
        // isPrime determines turn direction CW vs CCW
        HashMap<SquarePosition, SquarePosition> mappings = getTurnMappings(isPrime); 
        HashMap<SquarePosition, Square> newSquareMap = new HashMap<SquarePosition, Square>();
        
        // For each square position, get the current square, and map it to new position
        for (SquarePosition oldPosition : SquarePosition.values()) {
            
            // New position based on turn direction
            SquarePosition newPosition = mappings.get(oldPosition);
            
            // Preserve color, create new square object
            newSquareMap.put(newPosition, new Square(newPosition, getSquare(oldPosition).getColor()));
        }
        squareMap = newSquareMap;
    }
    
    public void insertSquares(HashMap<SquarePosition, Square> insertMap)
    {
        // At the given positions
        for (SquarePosition position : insertMap.keySet()) {            
            
            // Insert the given square color
            Color color = insertMap.get(position).getColor();
            squareMap.put(position, new Square(position, color));
        }
    }
    
    ///////////////////////////////////////
    //
    // Getters
    //
    ///////////////////////////////////////  
    
    public Square getTopLeftSquare()
    {
        return getSquare(SquarePosition.TOP_LEFT);
    }
    
    public Square getTopMiddleSquare()
    {
        return getSquare(SquarePosition.TOP_MIDDLE);
    }
    
    public Square getTopRightSquare()
    {
        return getSquare(SquarePosition.TOP_RIGHT);
    }
    
    public Square getCenterLeftSquare()
    {
        return getSquare(SquarePosition.CENTER_LEFT);
    }
    
    public Square getCenterMiddleSquare()
    {
        return getSquare(SquarePosition.CENTER_MIDDLE);
    }
    
    public Square getCenterRightSquare()
    {
        return getSquare(SquarePosition.CENTER_RIGHT);
    }
    
    public Square getBottomLeftSquare()
    {
        return getSquare(SquarePosition.BOTTOM_LEFT);
    }
    
    public Square getBottomMiddleSquare()
    {
        return getSquare(SquarePosition.BOTTOM_MIDDLE);
    }
    
    public Square getBottomRightSquare()
    {
        return getSquare(SquarePosition.BOTTOM_RIGHT);
    }
    
    ///////////////////////////////////////
    //
    //  Internals
    //
    ///////////////////////////////////////      
    
    protected Square getSquare(SquarePosition squarePosition)
    {
        return squareMap.get(squarePosition).clone();
    }
    
    protected static HashMap<SquarePosition, SquarePosition> getTurnMappings(boolean isPrime)
    {
        // Prime mappings
        HashMap<SquarePosition, SquarePosition> map = new HashMap<SquarePosition, SquarePosition>();
        map.put(SquarePosition.TOP_LEFT, SquarePosition.BOTTOM_LEFT);
        map.put(SquarePosition.TOP_MIDDLE, SquarePosition.CENTER_LEFT);
        map.put(SquarePosition.TOP_RIGHT, SquarePosition.TOP_LEFT);
        map.put(SquarePosition.CENTER_LEFT, SquarePosition.BOTTOM_MIDDLE);
        map.put(SquarePosition.CENTER_MIDDLE, SquarePosition.CENTER_MIDDLE);
        map.put(SquarePosition.CENTER_RIGHT, SquarePosition.TOP_MIDDLE);
        map.put(SquarePosition.BOTTOM_LEFT, SquarePosition.BOTTOM_RIGHT);
        map.put(SquarePosition.BOTTOM_MIDDLE, SquarePosition.CENTER_RIGHT);
        map.put(SquarePosition.BOTTOM_RIGHT, SquarePosition.TOP_RIGHT);
        
        HashMap<SquarePosition, SquarePosition> inverseMap = new HashMap<SquarePosition, SquarePosition>();
        
        // Non prime mappings
        for(Entry<SquarePosition, SquarePosition> entry : map.entrySet()){
            inverseMap.put(entry.getValue(), entry.getKey());
        }
        
        return !isPrime ? inverseMap : map;        
    }
    
    ///////////////////////////////////////
    //
    //  FaceType Enum
    //
    ///////////////////////////////////////
    
    public enum FaceType
    {
        TOP    (0),
        BOTTOM (1),
        RIGHT  (2),
        LEFT   (3),
        FRONT  (4),
        BACK   (5);
        
        int index;
                
        private FaceType(int index) {
            this.index = index;
        }
        
        public int getIndex() {
            return index;
        }
    }

}
