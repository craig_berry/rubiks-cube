
package rubiks;

import java.awt.Color;

public class Square
{
    SquarePosition squarePosition;
    Color color;

    public Square(SquarePosition squarePosition, Color color)
    {
        this.squarePosition = squarePosition;
        this.color = color;
    }
        
    public SquarePosition getSquarePosition()
    {
        return squarePosition;
    }

    public Color getColor()
    {
        return color;
    }    
    
    public Square clone() {
        return new Square(squarePosition, color);
    }

    ///////////////////////////////////////
    //
    //  SquareType Enum
    //
    ///////////////////////////////////////
    
    public enum SquarePosition
    {
        TOP_LEFT     (0),
        TOP_MIDDLE   (1),
        TOP_RIGHT    (2),
        CENTER_LEFT  (3),
        CENTER_MIDDLE(4),
        CENTER_RIGHT (5),
        BOTTOM_LEFT  (6),
        BOTTOM_MIDDLE(7),
        BOTTOM_RIGHT (8);
        
        int index;        
        
        private SquarePosition(int index) {
            this.index = index;
        }
        
        public int getIndex() {
            return index;
        }
    }
}
